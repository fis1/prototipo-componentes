import sys

##Se establece que va a importar del zip
sys.path.append("api/Api.zip")
# from paquete import clase
from Cargador import Cargador
from IEntrada import IEntrada
from ISalida import ISalida
from IRegla import IRegla


class Orquestador:
    def mostrar(self):

        if IEntrada.existeInstancia():
            IEntrada.recibirInformacion()
        if ISalida.existeInstancia():
            ISalida.desplegarInformacion()

        if IRegla.existeInstancia():
            IRegla.verificarReglas()

    ##Se le dice al cargador que importe la api
    def desempaquetarCargador(self):
        Cargador.importDinamico()


# Ejecucion del main, el punto de inicio en otras palabras
if __name__ == "__main__":
    orq = Orquestador()
    orq.desempaquetarCargador()
    orq.mostrar()
